package com.example.u6283016.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<String> notesArray = new ArrayList<>();
    int currentIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clearButtonPress(View v){
        EditText et =  findViewById(R.id.editText);


        if(notesArray.isEmpty()){
            currentIndex = -1; // reset the initial currentIndex
        }

        // store the content of edittext widget
        notesArray.add(et.getText().toString());


        // Clear the content of edittext widget
        et.setText("");
    }

    public void nextButtonPress(View v){
        String txt = findNext(false);

        EditText et = findViewById(R.id.editText);
        et.setText(txt);
    }

    public void deleteButtonPress(View v){
        String txt = findNext(true);

        EditText et = findViewById(R.id.editText);
        et.setText(txt);
    }

    private String findNext(boolean removeCurrent){
        currentIndex++;

        if(removeCurrent && !notesArray.isEmpty()) {
            currentIndex--;
            notesArray.remove(currentIndex);
        }


        if(currentIndex >= notesArray.size()){// fix the out of bound index
            currentIndex = 0;
        }

        // save the text to edittext widget
        if(!notesArray.isEmpty()) // save the next text
            return notesArray.get(currentIndex);
        else
           return ""; // return empty String if array is empty
    }

}
